# 1. Introduction
This project allows to use ESP32 as a simple wattmeter. ESP32 hosts a webpage and REST API for the wattmeter control over WiFi (Access Point or Station mode). 
Minimal external hardware is required (3 resistors). 2 internal ADC channels are used for voltage and current measuring.

Project is targeted for esp32-wroom-32d module on a development board, with built-in USB->UART converter. 
This ESP32 board only needs to be connected with micro USB cable to the PC. If you want to use another ESP32 board/module make sure pins RX, TX, BOOT and ENABLE pins are connected.

# 2. Hardware Setup
As mentioned, apart from the main ESP32, only 3 external resistors are required to have a working circuit: `R_1`, `R_2` and `R_s`.

## 2.1. Schematics
<img src="docs/schematics/schematic_any_load.png" width="600">

Schematic above allows to measure power usage of any component - here this component is represented by a DC motor.

## 2.2 Resistors selection

Resistor R_s is used as current sensing resistor. Its value should be chosen as:
```
R_s = U_max_adc / I_max
```
where `U_max_adc` is maximum voltage measured by the ESP32 ADC with 0 attenuation: `U_max_adc = 0.8 V`.
To small resistor value will cause lower resolution and increased noise. 
Too high value will not allow to measure `I_max` - current measurements will be trimmed at lower value. 
E.g if one wants to measure currents up to 500 mA, `R_s` resistance should be `1.6 Ω`.

`R_2` and `R_1` resistors allow to reduce the supply voltage to measurable range. The values can be calculated from the following formula:
```
R_1 + R_2 = 10kΩ  (may be also between 1kΩ and 20 kΩ)
R_1 / (R_1+R_2) * V_c = U_max_adc 
```

# 3. Usage overview
## 3.1. General examples
After starting the ESP32, one can connect to the AccessPoint (credentials in `src/main.cpp`) and go in the browse to the webpage `192.168.4.1`.
It will show the following webpage (hosted from ESP):

<img src="docs/screenshots/1_example_ui_recording__Screenshot_2020-11-09_15-00-15.png" width="600">

On the top of the page, one can select duration of the recording time, as well as samples decimation (decimation by averaging).
ESP32 samples the ADC with 10kHz timer internally, but due to limited RAM, it cannot save to much data. Samples limit is set to 20000.
Therefore, if one want to record 10 seconds of data, decimation should be set to 10.

Below, current value of the filtered voltage, current and power is displayed. This values are refreshed automatically every second.

In the middle, the main graph is presented. It uses [plotly.js](https://github.com/plotly/plotly.js/) library. The graph is interactive - can be zoomed and moved.

Underneath the graph, one can select plot type and force data reploting.

At the bottom of the page (not visible on the screen above) one can (and should) set values for the external resistors.

On the figure below one can see the power consumption graph example.

<img src="docs/screenshots/2_example_power_graph__Screenshot_2020-11-09_22-21-04.png" width="600">

Because ESP32 has also limited flash memory, one needs 8MB of flash to put the js libraries as static files. 
To allow working with more popular 4MB flashes, this js files are downloaded from the Interet at runtime. 
Therefore apart from connecting to ESP32 AccessPoint, one also needs another connection with Internet access.

Alternatively, one can switch to use ESP32 WiFi in stationary mode, and connect to an exisitng WiFi network (can be changed in `src/main.cpp`) 

## 3.2. IoT ESP32 current monitoring example 
As a more interesting example, power usage of an ESP32 was measured. 
The figure below shows one cycle of ESP32 based IoT device. 

<img src="docs/screenshots/3_esp_wifi_sending_cycle_ui___Screenshot_2020-11-09_23-11-11.png" width="600">

Until second ~0.5 the ESP is in deep sleep (power consumption around 15 uA).
Then it wakes up, connects to a WiFi network, sends 2 messages with measurements to the cloud (via MQTT protocol), disconnects and goes back to sleep.
This whole operation takes on average 1.3 s, which is also confirmed on the current consumption graph.

The `R_s` resistor value was a little bit too high, therefore the current is trimmed at ~140 mA. With a smaller resistor one can observe the full peak - up to around 400 mA, as visible on the figure below:

<img src="docs/screenshots/5_esp_wifi_sending_cycle__max_peak__Screenshot_2020-11-10_21-08-46.png" width="600">

But as one can see, this peaks are really short - around 10 ms, therefore the total energy consumption calculation is not really affected. 
This peaks are present due to WiFi usage.

On the figure below power consumption of the same cycle is presented - this time on mobile phone:

<img src="docs/screenshots/4_esp_wifi_sending_cycle__Screenshot_20201109-231740.png" width="600">

The last interesting graph is total `mA*s` consumption, which shows 2 operation cycles: 

<img src="docs/screenshots/6_esp_wifi_sending_cycle__mas__Screenshot_2020-11-09_23-07-19.png" width="600">

As one can see, one data sending cycle (to a cloud) consumes around 85 mAs.
It is especially interesting for an IoT device, powered from a battery with LDO, 
because energy consumption in `mA*s` can be used directly to calculate battery lifetime. 
E.g. a battery with 3200 mAh capacity, allows to perform 135 529 data sending cycles 
(`3200mAh * 3600s/h / 85 mAs/cycle`).

The hardware schematic used for these measurements is presented below:

<img src="docs/schematics/schematic_another_esp32_measurement.png" width="600">


## 3.3. Notes
- ESP32 ADC is not really high quality... Even after using the default calibration data.
- The ADC reading are especially nonlinear near 0 voltage, in the rest of the range the quality is quite good (checked with laboratory power supply).
- Therefore instead of zero value, a minimum reading may be measured (visible on the graphs as a minimum current while the ESP32 is in deep sleep).
The quality can be probably increased after recalibrating ESP32 ADC. It may be also due to a poor connections on the breadboard.
- But as long as one is aware of it and doesn't require high accuracy (less than 10% error) - it shouldn't be a problem

# 4. Development environment setup
## 4.1. First time configuration:
 - install prerequisities:

```
sudo apt-get install git wget flex bison gperf python python-pip python-setuptools cmake ninja-build ccache libffi-dev libssl-dev dfu-util
sudo apt-get install python3 python3-pip python3-setuptools
```
 - Add user to dialout group: `sudo usermod -a -G dialout $USER`
 - cd `$PROJECT_DIR`
 - Remember to update submodules: `git submodule update --init --recursive`
 - Install compiler: `./externals/esp-idf/install.sh`
## 4.2. After initial configuration
 - Create build directory and build:
```
mkdir $BUILD_DIR
cd $BUILD_DIR
. $PROJECT_DIR/externals/esp-idf/export.sh  # e.g.: . ../esp32_wattmeter/externals/esp-idf/export.sh
idf.py -B . -C $PROJECTS_DIR/ build  # e.g.: idf.py -B . -C ../esp32_wattmeter/ build
```
 - At this point you should be able to open the environment in QtCreator (remember to start the QtCreator from console with sourced `export.sh`)
 - To build, flash and monitor at once - run:
```
idf.py -B . -C ../esp32_wattmeter/ erase_flash build flash monitor -p /dev/ttyUSSB0
```
 - FLASHING NOTE:
 To flash ESP32 BOOT and RESET pins need to be drived. It may be done automatically by some converters. If it is not the case, press the button or drive the lines manually.

 - run menuconfig: `idf.py -B ./ -C $PROJECT_DIR menuconfig`
