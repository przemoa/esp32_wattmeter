#ifndef WIFI_CONTROL_H
#define WIFI_CONTROL_H


#include "esp_err.h"


namespace wifi
{

esp_err_t startAccessPoint(const char *apSsid, const char *apPassword);
esp_err_t connectToWifi(const char* ssid, const char* password);

}

#endif // WIFI_CONTROL_H
