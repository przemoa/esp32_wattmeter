static const char* LOG_TAG = "PowerMeasure";
#define LOG_LOCAL_LEVEL ESP_LOG_INFO


#include "power_measurement.h"

#include "defines.h"

#include <driver/adc.h>
#include <esp_adc_cal.h>
#include <soc/sens_reg.h>
#include <soc/sens_struct.h>
#include <sys/time.h>


namespace power_measurement
{

constexpr int MILLIVOLTS_IN_VOLT = 1000;
constexpr int MICROSECONDS_IN_MILLISECOND = 1000;
constexpr uint32_t MAX_NUMBER_OF_RECORDED_SAMPLES = 20000;


struct TPowerMeasurementControl
{
    bool isInitialized = false;
    esp_adc_cal_characteristics_t adc1Characteristics = {};

//    const int samplingPeriodUs = 100;
    const int samplingPeriodUs = 100;
    const float filteringFactor = 0.95f;

    float lastFilteredVoltageV = 0.0f;
    float lastFilteredCurrentA = 0.0f;
    float lastFilteredPowerW = 0.0f;

    float resistanceRs = 1.0f;  // R_sense resistor value [Ohm] - see schematic
    float resistanceR1R2Factor = 4.0; // Voltage measurement dividor - (R_1 + R_2) / R_2 [Ohm] - see schematic


    bool recordingInProgress = false;
    uint32_t samplesToRecord = 0;  // number of samples to be saved during recording
    uint32_t recordedSamples = 0;  // current index of recorded samples during recording

    uint16_t samplesAdcVoltageCorrected0[MAX_NUMBER_OF_RECORDED_SAMPLES] = {};
    uint16_t samplesAdcVoltageCorrected3[MAX_NUMBER_OF_RECORDED_SAMPLES] = {};

    uint32_t recordingSamplingDecimation = 0;  // if not every sample is recorded, the measurements will be decimated by averaging
    uint32_t recordingDecimationIterator = 0;  // how many subsamples have been already sumed for currently recorded sample
    uint32_t adcVoltageCorrected0Subsum = 0;
    uint32_t adcVoltageCorrected3Subsum = 0;


    int timerCallsCounter = 0;  // just for debugging purpose
};

static TPowerMeasurementControl pmc = {};

void setResistors(float resistanceRs, float resistanceR1R2Factor)
{
    pmc.resistanceRs = resistanceRs;
    pmc.resistanceR1R2Factor = resistanceR1R2Factor;
}

static int IRAM_ATTR local_adc1_read(int channel)
{
    // Code snippet from https://www.toptal.com/embedded/esp32-audio-sampling
    uint16_t adc_value;
    SENS.sar_meas_start1.sar1_en_pad = (1 << channel); // only one channel is selected
    while (SENS.sar_slave_addr1.meas_status != 0);
    SENS.sar_meas_start1.meas1_start_sar = 0;
    SENS.sar_meas_start1.meas1_start_sar = 1;
    while (SENS.sar_meas_start1.meas1_done_sar == 0);
    adc_value = SENS.sar_meas_start1.meas1_data_sar;
    return adc_value;
}

static void convertCorrectedAdcReadingsToMeasurements(
        uint32_t adcVoltageCorrected0,
        uint32_t adcVoltageCorrected3,
        float& voltageV,
        float& currentA)
{
    float supplyVoltageMvNotCorrected = adcVoltageCorrected0 * pmc.resistanceR1R2Factor;
    float supplyVoltageMv = supplyVoltageMvNotCorrected - adcVoltageCorrected3;

    voltageV = supplyVoltageMv / MILLIVOLTS_IN_VOLT;
    currentA = static_cast<float>(adcVoltageCorrected3) / MILLIVOLTS_IN_VOLT / pmc.resistanceRs;
}

static void convertRawAdcReadingsToMeasurements(
        uint32_t adcReadingRaw0,
        uint32_t adcReadingRaw3,
        float& voltageV,
        float& currentA)
{
    uint32_t adcVoltageCorrected0 = esp_adc_cal_raw_to_voltage(adcReadingRaw0, &pmc.adc1Characteristics);
    uint32_t adcVoltageCorrected3 = esp_adc_cal_raw_to_voltage(adcReadingRaw3, &pmc.adc1Characteristics);

    convertCorrectedAdcReadingsToMeasurements(adcVoltageCorrected0, adcVoltageCorrected3, voltageV, currentA);
}

static void processNewSample(uint32_t adcReadingRaw0, uint32_t adcReadingRaw3)
{
    float voltageV = 0.0f;
    float currentA = 0.0f;
    uint32_t adcVoltageCorrected0 = esp_adc_cal_raw_to_voltage(adcReadingRaw0, &pmc.adc1Characteristics);
    uint32_t adcVoltageCorrected3 = esp_adc_cal_raw_to_voltage(adcReadingRaw3, &pmc.adc1Characteristics);
    convertCorrectedAdcReadingsToMeasurements(adcVoltageCorrected0, adcVoltageCorrected3, voltageV, currentA);
    const float powerW = voltageV * currentA;

    pmc.lastFilteredCurrentA = pmc.lastFilteredCurrentA * pmc.filteringFactor + currentA * (1 - pmc.filteringFactor);
    pmc.lastFilteredVoltageV = pmc.lastFilteredVoltageV * pmc.filteringFactor + voltageV * (1 - pmc.filteringFactor);
    pmc.lastFilteredPowerW = pmc.lastFilteredPowerW * pmc.filteringFactor + powerW * (1 - pmc.filteringFactor);

    if (pmc.recordingInProgress)
    {
        DEBUG_ASSERT(pmc.recordedSamples < pmc.samplesToRecord);

        pmc.adcVoltageCorrected0Subsum += adcVoltageCorrected0;
        pmc.adcVoltageCorrected3Subsum += adcVoltageCorrected3;
        pmc.recordingDecimationIterator++;

        if (pmc.recordingDecimationIterator >= pmc.recordingSamplingDecimation)
        {
            pmc.samplesAdcVoltageCorrected0[pmc.recordedSamples] = static_cast<uint16_t>(pmc.adcVoltageCorrected0Subsum / pmc.recordingDecimationIterator);
            pmc.samplesAdcVoltageCorrected3[pmc.recordedSamples] = static_cast<uint16_t>(pmc.adcVoltageCorrected3Subsum / pmc.recordingDecimationIterator);

            pmc.adcVoltageCorrected0Subsum = 0;
            pmc.adcVoltageCorrected3Subsum = 0;
            pmc.recordingDecimationIterator = 0;
            pmc.recordedSamples++;
        }

        if (pmc.recordedSamples >= pmc.samplesToRecord)
        {
            pmc.recordingInProgress = false;
        }
    }
}

static void timerCallback(void* arg)
{
    uint32_t adcReadingRaw0 = static_cast<uint32_t>(local_adc1_read(ADC1_CHANNEL_0)); // executes ~ 1/80000 s, and can be called from ISR
    uint32_t adcReadingRaw3 = static_cast<uint32_t>(local_adc1_read(ADC1_CHANNEL_3));
    pmc.timerCallsCounter++;

    processNewSample(adcReadingRaw0, adcReadingRaw3);
}

static void setupAdc()
{
    LOG_INFO("About to setup ADC...");

    esp_adc_cal_value_t val_type = esp_adc_cal_characterize(
                ADC_UNIT_1,
                ADC_ATTEN_DB_0,
                ADC_WIDTH_BIT_12,
                1100,
                &pmc.adc1Characteristics);

    // Check type of calibration value used to characterize ADC
    if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF)
    {
        LOG_INFO("Available ADC calibration: eFuse Vref");
    }
    else if (val_type == ESP_ADC_CAL_VAL_EFUSE_TP)
    {
        LOG_INFO("Available ADC calibration: Two Point");
    }
    else
    {
        LOG_INFO("Available ADC calibration: Default");
    }

    adc1_config_width(ADC_WIDTH_BIT_12);
    adc1_config_channel_atten(ADC1_CHANNEL_0, ADC_ATTEN_DB_0);
    adc1_config_channel_atten(ADC1_CHANNEL_3, ADC_ATTEN_DB_0);

    // to force configuration with SDK code
    uint32_t adcReadingRaw0 = static_cast<uint32_t>(adc1_get_raw(ADC1_CHANNEL_0));  // executes ~ 1/25000 s, and cannot be called from ISR
    uint32_t adcReadingRaw3 = static_cast<uint32_t>(adc1_get_raw(ADC1_CHANNEL_3));

    convertRawAdcReadingsToMeasurements(
                adcReadingRaw0,
                adcReadingRaw3,
                pmc.lastFilteredVoltageV,
                pmc.lastFilteredCurrentA);

    LOG_INFO("ADC setup finished");
}

static void createAndStartTimer()
{
    const esp_timer_create_args_t periodic_timer_args =
    {
        .callback = &timerCallback,
        .name = "periodic"
    };

    esp_timer_handle_t periodic_timer;
    ESP_ERROR_CHECK(esp_timer_create(&periodic_timer_args, &periodic_timer));
    ESP_ERROR_CHECK(esp_timer_start_periodic(periodic_timer, 100));
}

//void init(TPowerMeasurementControl& powerMeasurementControl)
void init()
{
    if (pmc.isInitialized)
        return;

    setupAdc();
    createAndStartTimer();

    LOG_INFO("Initialization finished!");
    pmc.isInitialized = true;
}

bool getLastFilteredMeasurement(float& voltageV, float& currentA, float& powerW)
{
    if (!pmc.isInitialized)
    {
        LOG_ERROR("Module not initialized!");
        return false;
    }

    voltageV = pmc.lastFilteredVoltageV;
    currentA = pmc.lastFilteredCurrentA;
    powerW = pmc.lastFilteredPowerW;

    return true;
}

bool isRecordingInProgress()
{
    return pmc.recordingInProgress;
}

bool startRecodringData(int durationMs, uint32_t recordingSamplingDecimation)
{
    if (!pmc.isInitialized)
    {
        LOG_ERROR("Module not initialized!");
        return false;
    }

    pmc.recordingInProgress = false;
    SLEEP_MS(1);  // if the recording was in progress - wait for any operations on buffers to finish

    uint32_t samplesToRecord = durationMs * MICROSECONDS_IN_MILLISECOND / pmc.samplingPeriodUs / recordingSamplingDecimation;
    if (samplesToRecord > MAX_NUMBER_OF_RECORDED_SAMPLES)
    {
        LOG_ERROR("Cannot start recording - number of samples to record is too high! (%d > %d)",
                  samplesToRecord, MAX_NUMBER_OF_RECORDED_SAMPLES);
        return false;
    }

    pmc.recordingSamplingDecimation = recordingSamplingDecimation;
    pmc.samplesToRecord = samplesToRecord;
    pmc.recordingDecimationIterator = 0;
    pmc.adcVoltageCorrected0Subsum = 0;
    pmc.adcVoltageCorrected3Subsum = 0;
    pmc.recordedSamples = 0;

    pmc.recordingInProgress = true;

    return true;
}

void stopRecordingData()
{
    pmc.recordingInProgress = false;
    SLEEP_MS(1);  // if the recording was in progress - wait for any operations on buffers to finish

}

uint32_t getNumberOfRecordedMeasurements()
{
    return pmc.recordedSamples;
}

uint32_t getRecordedSamplesPeriodUs()
{
    return static_cast<uint32_t>(pmc.samplingPeriodUs) * pmc.recordingSamplingDecimation;
}

bool getRecordedMeasurement(uint32_t measurementNumber, float& voltageV, float& currentA)
{
    if (measurementNumber >= pmc.recordedSamples)
    {
        LOG_ERROR("Request measurement (number %d) doesn't exist!", measurementNumber);
        return false;
    }

    uint32_t adcVoltageCorrected0 = pmc.samplesAdcVoltageCorrected0[measurementNumber];
    uint32_t adcVoltageCorrected3 = pmc.samplesAdcVoltageCorrected3[measurementNumber];
    convertCorrectedAdcReadingsToMeasurements(adcVoltageCorrected0, adcVoltageCorrected3, voltageV, currentA);
    return true;
}


}
