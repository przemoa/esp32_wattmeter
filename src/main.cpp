static const char* LOG_TAG = "Main";
#define LOG_LOCAL_LEVEL ESP_LOG_INFO


#include "defines.h"
#include "power_measurement.h"
#include "web_server.h"
#include "wifi_control.h"

#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "soc/rtc.h"
#include "soc/sens_periph.h"
#include "driver/rtc_io.h"
#include "esp_sleep.h"
#include "nvs_flash.h"


static void printLastMeasurement()
{
    float voltageV = 0;
    float currentA = 0;
    float powerW = 0;
    power_measurement::getLastFilteredMeasurement(voltageV, currentA, powerW);
    LOG_INFO("Filtered reading: \t %f V, \t %f A,  \t %f W",
             static_cast<double>(voltageV),
             static_cast<double>(currentA),
             static_cast<double>(powerW));
}

static void initializeNVS()
{
    // Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
}

void mainCpp()
{
    initializeNVS();


    // Create Access Point
    static const char* AP_SSID = "ESP32-Wattmeter";
    static const char* AP_PASSWORD = "ppppqqqq";
    esp_err_t errorCode = wifi::startAccessPoint(AP_SSID, AP_PASSWORD);


    // Or connect to an existing WiFi
    static const char* STATION_SSID = "xxxx";
    static const char* STATION_PASSWORD = "xxxxxxxx";
    // wifi::connectToWifi(STATION_SSID, STATION_PASSWORD);


    web_server::start();

    power_measurement::init();

    while (1)
    {
        printLastMeasurement();
        SLEEP_MS(1000);
    }
}


extern "C"
{
void app_main(void)
{
    mainCpp();
}
}
