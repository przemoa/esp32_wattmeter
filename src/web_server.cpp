static const char* LOG_TAG = "WebServer";
#define LOG_LOCAL_LEVEL ESP_LOG_INFO


#include "web_server.h"

#include "defines.h"
#include "power_measurement.h"

#include <string.h>
#include <fcntl.h>
#include "esp_http_server.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_vfs.h"
#include "cJSON.h"

#include <string>


extern const char STATIC__INDEX_HTML[] asm("_binary_index_html_start");


namespace web_server
{

#define RESPONSE_BUFFER_LENGTH 25000
static char responseBuffer[RESPONSE_BUFFER_LENGTH + 1] = {0};


namespace request
{

const char START_RECORDING[] = "/start-recording";
const char RESISTORS[] = "/resistors";
const char GET_SAMPLES[] = "/samples";
const char GET_SAMPLES_BASE_PERIOD[] = "/samples-base-period";
const char GET_LastFilteredReading[] = "/last-filtered-reading";
const char GET_NUMBER_OF_SAMPLES[] = "/number-of-samples";
const char GET_MAIN_PAGE[] = "/index.html";

}


cJSON* getRequestJson(esp_err_t& error, httpd_req_t* req)
{
    error = ESP_OK;

    char content[300] = {};
    size_t recvSize = std::min(req->content_len, sizeof(content)-1);
    content[recvSize] = 0;

    int ret = httpd_req_recv(req, content, recvSize);
    LOG_INFO("Request %s. Content: %s", req->uri, content);
    if (ret <= 0)
    {
        /* 0 return value indicates connection closed */
        /* Check if timeout occurred */
        if (ret == HTTPD_SOCK_ERR_TIMEOUT)
        {
            httpd_resp_send_408(req);
        }
        error = ESP_FAIL;
        return nullptr;
    }

    cJSON *pJson = cJSON_Parse(content);
    return pJson;
}

esp_err_t getMainPage(httpd_req_t *req)
{
    /* Send a simple response */
    httpd_resp_send(req, STATIC__INDEX_HTML, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

bool handlePostStartRecording(cJSON *pJson, std::string& response)
{
    int durationMs = 0;
    int recordingSamplesDecimation = 0;

    // TODO: checking the exact types of numeric values and allowed values ranges
    // TODO: generic json parser and structures for request data. Separate parsing and control (execution).
    // TODO: also write tests for these...
    {
        const cJSON* durationMsJson = cJSON_GetObjectItemCaseSensitive(pJson, "durationMs");
        if (!cJSON_IsNumber(durationMsJson))
        {
            response = "Invalid field: 'durationMs'";
            return false;
        }
        durationMs = durationMsJson->valueint;
    }

    {
        const cJSON* recordingSamplesDecimationJson = cJSON_GetObjectItemCaseSensitive(pJson, "recordingSamplesDecimation");
        if (!cJSON_IsNumber(recordingSamplesDecimationJson))
        {
            response = "Invalid field: 'recordingSamplesDecimation'";
            return false;
        }
        recordingSamplesDecimation = recordingSamplesDecimationJson->valueint;
    }

    bool result = power_measurement::startRecodringData(durationMs, static_cast<uint32_t>(recordingSamplesDecimation));
    if (result)
    {
        response = "{\"status\": \"ok\"}";
    }
    else
    {
        response = "ERROR! startRecodringData failed";
    }

    return result;
}

bool handlePostResistors(cJSON *pJson, std::string& response)
{
    float resistanceRs = 0;
    float resistanceR1R2Factor = 0;

    {
        const cJSON* resistanceRsJson = cJSON_GetObjectItemCaseSensitive(pJson, "resistanceRs");
        if (!cJSON_IsNumber(resistanceRsJson))
        {
            response = "Invalid field: 'resistanceRs'";
            return false;
        }
        resistanceRs = static_cast<float>(resistanceRsJson->valuedouble);
    }

    {
        const cJSON* resistanceR1R2FactorJson = cJSON_GetObjectItemCaseSensitive(pJson, "resistanceR1R2Factor");
        if (!cJSON_IsNumber(resistanceR1R2FactorJson))
        {
            response = "Invalid field: 'resistanceR1R2Factor'";
            return false;
        }
        resistanceR1R2Factor = static_cast<float>(resistanceR1R2FactorJson->valuedouble);
    }

    power_measurement::setResistors(resistanceRs, resistanceR1R2Factor);
    response = "{\"status\": \"ok\"}";
    return true;
}


esp_err_t handlePostRequest(httpd_req_t* req)
{
    esp_err_t error;
    cJSON* pJson = getRequestJson(error, req);
    if (error != ESP_OK)
    {
        DEBUG_ASSERT(pJson == nullptr);
        return error;
    }

    if (pJson == nullptr)
    {
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Failed to parse request content to JSON");
        return ESP_OK;
    }

    bool result = false;
    std::string response = "";

    if (strcmp(req->uri, request::START_RECORDING) == 0)
    {
        result = handlePostStartRecording(pJson, response);
    }
    else if (strcmp(req->uri, request::RESISTORS) == 0)
    {
        result = handlePostResistors(pJson, response);
    }
    else
    {
        response = "Unknwon post request!";
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, response.c_str());
        cJSON_Delete(pJson);
        return ESP_OK;
    }


    if (result)
    {
        LOG_INFO("Sending response: %s", response.c_str());
        httpd_resp_send(req, response.c_str(), HTTPD_RESP_USE_STRLEN);
    }
    else
    {
        LOG_ERROR("Failed to process request: %s", response.c_str());
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, response.c_str());
    }

    cJSON_Delete(pJson);
    return ESP_OK;
}

esp_err_t getNumberOfSamples(httpd_req_t *req)
{
    uint32_t numberOfSamples = power_measurement::getNumberOfRecordedMeasurements();
    snprintf(responseBuffer, RESPONSE_BUFFER_LENGTH, "{\"numberOfSamples\": %u}", numberOfSamples);

    httpd_resp_set_type(req, "application/json");
    httpd_resp_send(req, responseBuffer, HTTPD_RESP_USE_STRLEN);

    return ESP_OK;
}

esp_err_t getSamplesBasePeriod(httpd_req_t *req)
{
    uint32_t samplingPeriodUs = power_measurement::getRecordedSamplesPeriodUs();
    snprintf(responseBuffer, RESPONSE_BUFFER_LENGTH, "{\"samplingPeriodUs\": %u}", samplingPeriodUs);

    httpd_resp_set_type(req, "application/json");
    httpd_resp_send(req, responseBuffer, HTTPD_RESP_USE_STRLEN);

    return ESP_OK;
}

esp_err_t getLastFilteredReading(httpd_req_t *req)
{
    float voltageV = 0;
    float currentA = 0;
    float powerW = 0;
    power_measurement::getLastFilteredMeasurement(voltageV, currentA, powerW);
    snprintf(responseBuffer, RESPONSE_BUFFER_LENGTH, "{\"voltageV\": %f, \"currentA\": %f, \"powerW\": %f}",
             static_cast<double>(voltageV), static_cast<double>(currentA), static_cast<double>(powerW));

    httpd_resp_set_type(req, "application/json");
    httpd_resp_send(req, responseBuffer, HTTPD_RESP_USE_STRLEN);

    return ESP_OK;
}

bool setSamplesResponseContent(char* buffer, int bufferLength, int startSample, int endSample)
{
    int remainingBufferLength = bufferLength;

    int bytesWritten = snprintf(buffer, static_cast<size_t>(remainingBufferLength), "{\"startSample\": %d, \"data\": [", startSample);
    if (bytesWritten <= 0)
        return false;
    buffer += bytesWritten;
    remainingBufferLength -= bytesWritten;


    for (int sampleNo = startSample; sampleNo <= endSample; sampleNo++)
    {
        float voltageV = 0;
        float currentA = 0;

        if (!power_measurement::getRecordedMeasurement(static_cast<uint32_t>(sampleNo), voltageV, currentA))
            return false;

        LOG_DEBUG("Processing sample %d: V,A = %f, %f", sampleNo, voltageV, currentA);

        bytesWritten = snprintf(buffer, static_cast<size_t>(remainingBufferLength), "[%.3f,%.5f],",
                                static_cast<double>(voltageV), static_cast<double>(currentA));
        if (bytesWritten <= 0)
            return false;
        buffer += bytesWritten;
        remainingBufferLength -= bytesWritten;

        if (sampleNo == endSample)
        {
            // remove trailing ','
            *buffer = 0;
            buffer--;
            remainingBufferLength++;
        }
    }

    bytesWritten = snprintf(buffer, static_cast<size_t>(remainingBufferLength), "]}");
    if (bytesWritten <= 0)
        return false;
    buffer += bytesWritten;
    remainingBufferLength -= bytesWritten;

    return true;
}

bool parseGetSamplesString(cJSON* pJson, int& startSample, int& endSample, std::string& response)
{
    {
        const cJSON* startSampleJson = cJSON_GetObjectItemCaseSensitive(pJson, "startSample");
        if (!cJSON_IsNumber(startSampleJson))
        {
            response = "Invalid field: 'startSample'";
        }
        startSample = startSampleJson->valueint;
    }
    {
        const cJSON* endSampleJson = cJSON_GetObjectItemCaseSensitive(pJson, "endSample");
        if (!cJSON_IsNumber(endSampleJson))
        {
            response = "Invalid field: 'endSample'";
            return false;
        }
        endSample = endSampleJson->valueint;
    }

    return true;
}

esp_err_t getSamples(httpd_req_t *req)
{
    esp_err_t error;
    cJSON* pJson = getRequestJson(error, req);
    if (error != ESP_OK)
    {
        DEBUG_ASSERT(pJson == nullptr);
        return error;
    }

    if (pJson == nullptr)
    {
        LOG_ERROR("Failed to parse JSON");
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Failed to parse request content to JSON");
        return ESP_OK;
    }

    int startSample = 0;
    int endSample = 0;
    std::string response = "";

    if (!parseGetSamplesString(pJson, startSample, endSample, response))
    {
        LOG_ERROR("Invalid json: %s", response.c_str());
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, response.c_str());
    }

    LOG_INFO("Getting samples from %d to %d...", startSample, endSample);

    bool result = setSamplesResponseContent(responseBuffer, RESPONSE_BUFFER_LENGTH, startSample, endSample);
    if (!result)
    {
        LOG_ERROR("Failed to setSamplesResponseContent!");
        snprintf(responseBuffer, RESPONSE_BUFFER_LENGTH, "ERROR! Failed to setSamplesResponseContent!");
    }

    httpd_resp_set_type(req, "application/json");
    LOG_INFO("Response length: %u", strlen(responseBuffer));
    httpd_resp_send(req, responseBuffer, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

/* URI handler structures */
httpd_uri_t uirHandlers[] = {
    {
        .uri      = request::GET_SAMPLES,
        .method   = HTTP_POST,  // post only to allow for request body...
        .handler  = getSamples,
        .user_ctx = nullptr
    },
    {
        .uri      = request::GET_NUMBER_OF_SAMPLES,
        .method   = HTTP_GET,
        .handler  = getNumberOfSamples,
        .user_ctx = nullptr
    },
    {
        .uri      = request::GET_SAMPLES_BASE_PERIOD,
        .method   = HTTP_GET,
        .handler  = getSamplesBasePeriod,
        .user_ctx = nullptr
    },
    {
        .uri      = request::GET_LastFilteredReading,
        .method   = HTTP_GET,
        .handler  = getLastFilteredReading,
        .user_ctx = nullptr
    },
    {
        .uri      = request::GET_MAIN_PAGE,
        .method   = HTTP_GET,
        .handler  = getMainPage,
        .user_ctx = nullptr
    },
    {
        .uri      = "/",
        .method   = HTTP_GET,
        .handler  = getMainPage,
        .user_ctx = nullptr
    },
    {
        .uri      = request::START_RECORDING,
        .method   = HTTP_POST,
        .handler  = handlePostRequest,
        .user_ctx = nullptr
    },
    {
        .uri      = request::RESISTORS,
        .method   = HTTP_POST,
        .handler  = handlePostRequest,
        .user_ctx = nullptr
    }
};

/* Function for starting the webserver */
httpd_handle_t start_webserver(void)
{
    /* Generate default configuration */
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    httpd_handle_t server = nullptr;

    if (httpd_start(&server, &config) == ESP_OK)
    {
        /* Register URI handlers */
        int numberOfUriHandlers = sizeof (uirHandlers) / sizeof (uirHandlers[0]);
        for (int i = 0; i < numberOfUriHandlers; i++)
        {
            httpd_register_uri_handler(server, &uirHandlers[i]);
        }
    }

    /* If server failed to start, handle will be NULL */
    return server;
}

bool start()
{
    const char* ssid = "PrzemLink";
    const char* password =  "PrzemekFajny611";


    start_webserver();

    return true;
}

}

