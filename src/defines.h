#ifndef DEFINES_H
#define DEFINES_H

#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "stdint.h"


#define ENABLE_DEBUG_LOGGING
#ifdef ENABLE_DEBUG_LOGGING
    #define LOG_DEBUG(...)       ESP_LOGD(LOG_TAG, __VA_ARGS__)
    #define LOG_INFO(...)        ESP_LOGI(LOG_TAG, __VA_ARGS__)
    #define LOG_WARNING(...)     ESP_LOGW(LOG_TAG, __VA_ARGS__)
    #define LOG_ERROR(...)       ESP_LOGE(LOG_TAG, __VA_ARGS__)
#else
    #define LOG_DEBUG(...)       do {} while(0)
    #define LOG_INFO(...)        do {} while(0)
    #define LOG_WARNING(...)     ESP_LOGW(LOG_TAG, __VA_ARGS__)
    #define LOG_ERROR(...)       ESP_LOGE(LOG_TAG, __VA_ARGS__)
#endif


#define IS_DEBUG_BUILD
#ifdef IS_DEBUG_BUILD
    #define DEBUG_ASSERT(x) do { if (!(x)) { \
        LOG_ERROR("Assertion failed. Function %s, file %s, line %d.\n", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
        while (1) { } } } while(0)
#else
    #define DEBUG_ASSERT(x) do {} while(0)
#endif


#define SLEEP_MS(X_MS) vTaskDelay(((X_MS >= 10) ? X_MS : 10) / portTICK_PERIOD_MS)



#endif // DEFINES_H
