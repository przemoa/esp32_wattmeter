#ifndef POWER_MEASUREMENT_H
#define POWER_MEASUREMENT_H


#include <stdint.h>


namespace power_measurement
{

//void init(TPowerMeasurementControl& powerMeasurementControl)
void init();

bool getLastFilteredMeasurement(float& voltageV, float& currentA, float& powerW);

void setResistors(float resistanceRs, float resistanceR1R2Factor);


bool isRecordingInProgress();

bool startRecodringData(int durationMs, uint32_t recordingSamplingDecimation);

void stopRecordingData();

uint32_t getNumberOfRecordedMeasurements();

uint32_t getRecordedSamplesPeriodUs();

bool getRecordedMeasurement(uint32_t measurementNumber, float& voltageV, float& currentA);


}

#endif // POWER_MEASUREMENT_H
